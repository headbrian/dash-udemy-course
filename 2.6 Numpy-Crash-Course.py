# Numpy crash course
import numpy as np

a=np.arange(30,100)
b=np.random.randint(0,100)

np.random.seed(101)
b=np.random.randint(0,100)
print(b)


x=np.linspace(0, 80, 300)
print(x)

x.min()
x.max()
x.argmin()
x.argmax()

x.size
y=x.reshape(15,20)
y
mat=np.arange(100).reshape(10,10)

# return row 5 column 2
mat[5,2]
# return all rows for column 2
mat[:,2]

mat > 50
mat[mat > 50]
##


