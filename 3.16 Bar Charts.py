import pandas as pd
import plotly.offline as pyo
import plotly.graph_objects as go
##

df = pd.read_csv('../Plotly-Dashboards-with-Dash/data/2018WinterOlympics.csv')

trace1 = go.Bar(x=df['NOC'],
                y=df['Gold'],
                name='Gold',
                marker=dict(color='#DAA520')
                )
trace2 = go.Bar(x=df['NOC'],
                y=df['Silver'],
                name='Silver',
                marker=dict(color='#C0C0C0')
                )
trace3 = go.Bar(x=df['NOC'],
                y=df['Bronze'],
                name='Bronze',
                marker=dict(color='#CD7F32')
                )

data=[trace3,trace2,trace1]

layout=go.Layout(title='Medals')
layout=go.Layout(title='Medals', barmode='stack')

fig=go.Figure(data=data,layout=layout)
pyo.plot(fig)

# Exercise
#######
# Objective: Create a stacked bar chart from
# the file ../data/mocksurvey.csv. Note that questions appear in
# the index (and should be used for the x-axis), while responses
# appear as column labels.  Extra Credit: make a horizontal bar chart!
######

# Perform imports here:
import pandas as pd
import plotly.offline as pyo
import plotly.graph_objects as go

# create a DataFrame from the .csv file:
df = pd.read_csv('../Plotly-Dashboards-with-Dash/data/mocksurvey.csv',index_col=0)

# create traces using a list comprehension:

data = [go.Bar(y=df.index,
                x=df[x],
                name=x,
               orientation='h'
                ) for x in df.columns]

# create a layout, remember to set the barmode here
layout=go.Layout(title='Survey monkey',barmode='stack')

# create a fig from data & layout, and plot the fig.
fig=go.Figure(data=data,layout=layout)
# pyo.plot(fig)


##

import plotly.io as pio
pio.renderers.default='png'
fig.show()

##
# Heat Map
# 121148318
import pandas as pd
import plotly.offline as pyo
import plotly.graph_objects as go

# create a DataFrame from the .csv file:
df = pd.read_csv('C:/Temp/DATA6inch.csv')


data=[go.Heatmap(x=df['X'], y=df['Y'], z=df['parm1'].values.tolist())]

layout=go.Layout(title='Wafer Map')

fig= go.Figure(data=data,layout=layout)

pyo.plot(fig)