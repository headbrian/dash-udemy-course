import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc

import dash_chart1 as c1
import Components as c
import styles as s

##
PLOTLY_LOGO = "https://images.plot.ly/logo/new-branding/plotly-logomark.png"

app = dash.Dash(external_stylesheets=[dbc.themes.JOURNAL])

plotdata = c1.plot_flg(['MFG532A', 'MFG337A', 'MFG532A', 'MFG406A', 'MFG10A'])
chartsperrow = 2

charts = []
row = []
for ctr in range(len(plotdata)):
    row.append(dbc.Col(dcc.Graph(id='example' + str(ctr),
                                 figure=dict(data=plotdata[ctr][0], layout=plotdata[ctr][1]))
                       , width=12/chartsperrow)
               )
    if (ctr % chartsperrow == chartsperrow - 1) or (
            (len(plotdata) % chartsperrow == chartsperrow - 1) & (ctr + 1 == len(plotdata))):
        charts.append(html.P())
        charts.append(dbc.Row(row, justify="center"))
        row = []

content = html.Div(html.Div(charts), id="page-content", style=s.CONTENT_STYLE)

app.layout = html.Div([c.navbar, c.sidebar, content])

# app.layout = html.Div([c.navbar,
#                        ])

if __name__ == '__main__':
    app.run_server()
