import numpy as np
import plotly.offline as pyo
import plotly.graph_objects as go

##
np.random.seed(42)
random_x = np.random.randint(1, 101, 100)
random_y = np.random.randint(1, 101, 100)
data = [go.Scatter(x=random_x,
                   y=random_y,
                   mode='markers',
                   marker=dict(size=12,
                               color='rgb(52,204,153)',
                               symbol='pentagon',
                               line={'width':2}
                                ))]
pyo.plot(data, filename='scatter.html')

# Add Layout
layout = go.Layout(title='Hello First Plot',
                   xaxis={'title': 'MY X AXIS'},
                   yaxis=dict(title='MY Y AXIS'),
                   hovermode='closest'
                   )
fig=go.Figure(data=data, layout=layout)
pyo.plot(fig, filename='scatter.html')


##
np.random.seed(56)

x_values = np.linspace(0,1,100)
y_values = np.random.randn(100)

trace0=go.Scatter(x=x_values,
                 y=y_values+5,
                 mode='markers',
                 name='markers')
trace1=go.Scatter(x=x_values,
                 y=y_values,
                 mode='lines',
                 name='mylines')
trace2=go.Scatter(x=x_values,
                 y=y_values-5,
                 mode='lines+markers',
                 name='line+markers')

data=[trace0,trace1,trace2]
la=go.Layout(title='Line Charts')
fig=go.Figure(data=data,layout=la)
pyo.plot(fig,filename='line.html')

##

import pandas as pd

df=pd.read_csv('../Plotly-Dashboards-with-Dash/SourceData/nst-est2017-alldata.csv')
df2 = df[df['DIVISION'] == '1']
df2.set_index('NAME',inplace=True)

list_of_pop_col = [col for col in df2.columns if col.startswith('POP')]
# print(list_of_pop_col)
df2 = df2[list_of_pop_col]

data=[go.Scatter(x=df2.columns,y=df2.loc[name],mode='lines',name=name) for name in df2.index]
pyo.plot(data)

