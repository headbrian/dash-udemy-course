import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import styles as s
#%%
# daologo='daologo.png'

navbar = dbc.NavbarSimple(
    children=[
        dbc.NavItem(dbc.NavLink("Error Codes", active=True, href="#")),
        dbc.NavItem(dbc.NavLink("Wafer Map", disabled=True, href="#")),
        dbc.DropdownMenu(
            children=[
                dbc.DropdownMenuItem("More pages", header=True),
                dbc.DropdownMenuItem("Page 2", href="#"),
                dbc.DropdownMenuItem("Page 3", href="#"),
            ],
            nav=True,
            in_navbar=True,
            label="More",
        ),
    ],
    brand="DAO-Solution Management",
    brand_href="#",
    color="dark",
    dark=True,
    style=s.NAVBAR_STYLE
)

email_input = dbc.FormGroup(
    [
        dbc.Label("Email", html_for="example-email"),
        dbc.Input(type="email", id="example-email", placeholder="Enter email"),
        dbc.FormText(
            "Are you on email? You simply have to be these days"
        ),
    ]
)

password_input = dbc.FormGroup(
    [
        dbc.Label("Password", html_for="example-password"),
        dbc.Input(
            type="password",
            id="example-password",
            placeholder="Enter password",
        ),
        dbc.FormText(
            "A password stops mean people taking your stuff"
        ),
    ]
)

form = dbc.Form([email_input, password_input])


sidebar = html.Div(
    [form

    ],
    style=s.SIDEBAR_STYLE,
)