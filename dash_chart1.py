import config
# import pandas as pd
import plotly.offline as pyo
import plotly.graph_objects as go
import datetime as dt

flg = config.extract_data()
##
def plot_flg(tools):
    out = []
    for tool in tools:
        # minhr1 = (dt.datetime.now() - dt.timedelta(hours=1)).strftime("%Y-%m-%d %H:%M")
        minhr1 = (dt.datetime.now() - dt.timedelta(days=3.5)).strftime("%Y-%m-%d %H:%M")
        t = flg[(flg['tool_id'] == tool) & (flg['toolts'] > minhr1)]

        data = [go.Scatter(x=t['toolts'],
                           y=t[t['blade_id'] == b]['error_code'],
                           mode='lines+markers',
                           name=b) for b in t['blade_id'].unique()]

        layout = go.Layout(title='Flaggy Error Codes - Tool {}'.format(tool),
                           template='plotly_dark',
                           )

        out.append([data, layout])

    return out


# fig = go.Figure(data=pt[0][0], layout=pt[0][1])
# pyo.plot(fig)
##
# tool='MFG10A'
# minhr1 = (dt.datetime.now() - dt.timedelta(days=3.5)).strftime("%Y-%m-%d %H:%M")
# t = flg[(flg['tool_id'] == tool) & (flg['toolts'] > minhr1)]
#
# data = [go.Scatter(x=t[t['error_code']==v]['toolts'],
#                    y=t[t['error_code']==v]['blade_id'],
#                    name='Error code' + str(t[t['error_code']==v]['error_code'].unique()),
#                    mode='markers',
#                    showlegend=True,
#                    # marker=dict(),
#                     marker_symbol = 'line-ns-open'
#                    ) for v in t['error_code'].unique()]
#
# layout = go.Layout(title='Flaggy Error Codes - Tool {}'.format(tool),
#                    template='plotly_dark',
#                    )
#
# fig = go.Figure(data=data, layout=layout)
# pyo.plot(fig)
